<!DOCTYPE html>
<html class="<?php echo wcdn_get_template_type(); ?>">
<head>
	<meta charset="utf-8">
	<title><?php wcdn_template_title(); ?></title>
	<?php wcdn_head(); ?>
	<link rel="stylesheet" href="<?php wcdn_stylesheet_url( 'style.css' ); ?>" type="text/css" media="screen,print" />
</head>
<body>
	<div id="container">
		<?php wcdn_navigation(); ?>
		<div id="content">
			<div id="page">
				<div style="min-height:200px;">
				<div id="letter-header">
					<div class="heading"></div>				
				</div><!-- #letter-header -->
				
				<div style="padding: 20px 0;">
					<div style="float:left; font-size: 20px; line-height: 20px;">
						<h2> Office Address </h2>
						It’s A Secret<br/>
						15 rue Nash<br/>
						Dollard-des-Ormeaux, QC<br/>
						H9B 2N9<br/>
						Canada
					</div>
									<br clear="all"/>
									
					<div style="width: 350px; margin: 0 auto; margin-top: 70px; font-size: 27px;line-height: 27px;">
					
						<h2> Recipient Address </h2>
							<?php if( wcdn_get_template_type() == 'invoice' ) : ?>
								<?php wcdn_billing_address(); ?>
							<?php else : ?>
								<?php wcdn_shipping_address(); ?>
							<?php endif ?>
					</div>
				</div>
				<div class="clear:both" ></div>
				</div>
				
			</div><!-- #page -->
		</div><!-- #content -->
	</div><!-- #container -->
</body>
</html>