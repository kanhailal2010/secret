<?php
/*
Plugin Name: Wordpress Limited Posts on Page
Plugin URI: http://www.tbinfotech.juplo.com
Description: Wordpress plugin for Limited Posts on Page. ShortCode is [limited_posts_on_page]
Author: Rajesh Thanoch
Version: 1.0
Author URI: http://www.tbinfotech.juplo.com
*/

$siteurl = get_option('siteurl');
define('PLUGIN_FOLDER', dirname(plugin_basename(__FILE__)));
define('PLUGIN_URL', $siteurl.'/wp-content/plugins/' . PLUGIN_FOLDER);
define('PLUGIN_FILE_PATH', dirname(__FILE__));
define('PLUGIN_DIR_NAME', basename(PLUGIN_FILE_PATH));

register_activation_hook(__FILE__,'');
register_deactivation_hook(__FILE__ , '' );

function limited_admin_post()
{
	 include 'limited_post.php';
}

//Add ShortCode for "front end listing"
add_shortcode("limited_posts_on_page","limited_post_shortcode");
 function limited_post_shortcode($atts) 
{ 
	  include 'limited_post.php';
}
?>