<?php
/**
 * My Account page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce;

$woocommerce->show_messages(); ?>

<p class="myaccount_user">
	<?php
	printf(
		__( 'Welcome to your account  <strong>%s</strong>.You may use this page to manage your orders, payments, and other information, all in one place. ', 'woocommerce' ),
		$current_user->display_name,
		get_permalink( woocommerce_get_page_id( 'change_password' ) )
	);
	?>
</p>
<style type="text/css">
 .my-account-div{width:1000px;}
 .order-div {width:560px; margin: 0 auto; background:rgb(238,238,238); margin-top:15px;}	
 .main-class{width:100%; float:left; backgroud:none;}
 .row{float:left; width:100%; margin-top:-1px;}
 .left-div, .right-div{float:left; width:48%; height:21px; border:.5px solid #666666; padding: 4px;}
 .right-div{margin-left:-1px;}
 .heading-div{background: none repeat scroll 1px center #666666;
    font-weight: bold;
    padding: 5px;
    width: 97.5%;
	color:#FFF;}
	.clr{clear:both;}
	.logout-link{ text-align: center; width:1000px;}
	a{color:#F26E59; text-decoration:none;}
</style>
<div  class="my-account-div">
<div class="order-div">
	<div class="heading-div">
	My orders
	</div>
	<div class="clr"> </div>
	<div class="main-class">
		<div class="row">
			<div class="left-div">
				<a href="<?php echo home_url(); ?>/?page_id=17">Review orders / track packages</a>
			</div>
			<div class="right-div">
			<a href="<?php echo home_url(); ?>/">Change quantities / cancel orders </a>
			</div>
		</div>
		<div class="clr"> </div>
		<div class="row">
			<div class="left-div">
				<a href="<?php echo home_url(); ?>/?page_id=16&lang=en&address=shipping">Change shipping address </a>
			</div>
			<div class="right-div">
			<a href="<?php echo home_url(); ?>/">Print invoices </a>
			</div>
		</div>
		<div class="clr"> </div>
		<div class="row">
			<div class="left-div">
			<a href="<?php echo home_url(); ?>/?page_id=16&lang=en&address=billing">	Change billing address </a>
			</div>
			<div class="right-div">
			<a href="<?php echo home_url(); ?>/?page_id=255&lang=en"> Return items</a>
			</div>
		</div>
		<div class="clr"> </div>
	</div>
		
</div>
<div class="clr"> </div>
<div class="order-div">
	<div class="heading-div">
	Personal Information
	</div>
	<div class="clr"> </div>
	<div class="main-class">
		<div class="row">
			<div class="left-div">
				<a href="<?php echo home_url(); ?>/?page_id=18">Change e-mail passwords</a>
			</div>
			<div class="right-div">
			<a href="<?php echo home_url(); ?>/?page_id=16&lang=en&address=billing">Manage your billing addresses </a>
			</div>
		</div>
		<div class="clr"> </div>
		<div class="row">
			<div class="left-div">
				<a href="<?php echo home_url(); ?>/?page_id=15">Forgot e-mail Password ?</a>
			</div>
			<div class="right-div">
			<a href="<?php echo home_url(); ?>/?page_id=16&lang=en&address=shipping">Manage your shipping addresses</a>
			</div>
		</div>
		
		<div class="clr"> </div>
	</div>
		
</div>
<div class="clr"> </div>


<div class="order-div">
	<div class="heading-div">
	Payment Settings
	</div>
	<div class="clr"> </div>
	<div class="main-class">
		<div class="row">
			<div class="left-div">
				<a href="<?php echo home_url(); ?>/?page_id=6&amp;lang=en">Edit or delete a credit / debit card</a>
			</div>
			<div class="right-div">
			<!--<a href="<?php echo home_url(); ?>/">/?itsasecret/testin/> View gift certificate balance-->
			<a href="http://webdeskers.com/itsasecret/testin/">View gift certificate balance</a>
			
			</a>
			</div>
		</div>
		<div class="clr"> </div>
		<div class="row">
			<div class="left-div">
				<a href="<?php echo home_url(); ?>/"></a>
			</div>
			<div class="right-div">
			<a href="<?php echo home_url(); ?>/">Apply a gift certificate to your account</a>
			</div>
		</div>
		
		<div class="clr"> </div>
	</div>
		
</div>
<div class="clr"> </div>

<div class="order-div">
	<div class="heading-div">
	Other Features
	</div>
	<div class="clr"> </div>
	<div class="main-class">
		<div class="row">
			<div class="left-div">
				<a href="<?php echo home_url(); ?>/">Edit a review that I wrote</a>
			</div>
			<div class="right-div">
			<a href="<?php echo home_url(); ?>/?page_id=55">View my Wish List </a>
			</div>
		</div>
		<div class="clr"> </div>
	</div>
		
</div>
<div class="clr"> </div>
<div class="logout-link"> <a href="<?php echo home_url(); ?>/?page_id=14&amp;lang=en"><!--I'm done managing my account, log me out.--></a></div>
</div> <!--
<?php do_action( 'woocommerce_before_my_account' ); ?>

<?php woocommerce_get_template( 'myaccount/my-downloads.php' ); ?>

<?php  woocommerce_get_template( 'myaccount/my-orders.php', array( 'order_count' => $order_count ) ); ?>
<h1> Change Your Password </h1>
<?php echo do_shortcode('[woocommerce_change_password]'); ?>

<?php  woocommerce_get_template( 'myaccount/my-address.php' ); ?>
<h1>Wish List </h1>
<?php  echo do_shortcode('[yith_wcwl_wishlist]'); ?>'

<?php  do_action( 'woocommerce_after_my_account' ); ?> -->