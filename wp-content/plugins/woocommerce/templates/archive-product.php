<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author     	WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

get_header('shop'); ?>
</div>
<!-- header_close -->
    	<!-- contaner -->
<div class="contaner">

<?php
        
		/**
		 * woocommerce_before_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 /* @hooked woocommerce_breadcrumb - 20
		 */
	do_action('woocommerce_before_main_content');
	?>

		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			<h1 class="page-title"><?php // woocommerce_page_title(); ?></h1>

		<?php endif; ?>
    <?php
     if (is_product_category('14')){
        ?>
         <img src="<?php echo home_url(); ?>/wp-content/uploads/2013/07/1818.jpg"/>
        <?php
     }
     elseif (is_product_category('17')){
     ?>
     <img src="<?php echo home_url(); ?>/wp-content/uploads/2013/07/1819.jpg"/>
     <?php
     
     } 
      elseif (is_product_category('16')){
     ?>
     <img src="<?php echo home_url(); ?>/wp-content/uploads/2013/07/1820.jpg"/>
     <?php
     
     } 
      elseif (is_product_category('18')){
     ?>
     <img src="<?php echo home_url(); ?>/wp-content/uploads/2013/07/1817.jpg"/>
     <?php
     
     } 
     ?>
     
		<?php do_action( 'woocommerce_archive_description' ); ?>
        <?php
        if (is_product_category()){
    global $wp_query;
    // get the query object
    $cat = $wp_query->get_queried_object();
    // get the thumbnail id user the term_id
    $thumbnail_id = get_woocommerce_term_meta( $cat->term_id, 'thumbnail_id', true ); 
    // get the image URL
    $image = wp_get_attachment_url( $thumbnail_id ); 
    // print the IMG HTML
    echo '<img src="'.$image.'" alt=""  />';
    
}
?>
<div style="clear:both"> </div>
		<?php if ( have_posts() ) : ?>

			<?php
				/**
				 * woocommerce_before_shop_loop hook
				 *
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
			do_action( 'woocommerce_before_shop_loop' );
			?>

			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( have_posts() ) : the_post(); ?>

					<?php woocommerce_get_template_part( 'content', 'product' ); ?>
                        
				<?php endwhile; // end of the loop. ?>

			<?php woocommerce_product_loop_end(); ?>

			<?php
				/**
				 * woocommerce_after_shop_loop hook
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
			?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php woocommerce_get_template( 'loop/no-products-found.php' ); ?>

		<?php endif; ?>

	<?php
		/**
		 * woocommerce_after_main_content hook
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action('woocommerce_after_main_content');
	?>

	<?php
		/**
		 * woocommerce_sidebar hook
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
	//	do_action('woocommerce_sidebar');
	?>
      <?php
     if (is_product_category('14')){
        ?>
    <iframe width="640" height="360" src="http://www.youtube.com/embed/wwBFHQ95OWc?feature=player_embedded" frameborder="0" allowfullscreen></iframe>
<?php } 
 elseif (is_product_category('42')){
        ?>
    <iframe width="640" height="360" src="http://www.youtube.com/embed/wwBFHQ95OWc?feature=player_embedded" frameborder="0" allowfullscreen></iframe>
<?php } ?>
</div>


<?php get_footer('shop'); ?>