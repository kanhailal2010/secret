<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
//echo $test;

global $post, $product;

?>
<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="k_product_price_div" >
	<table>
		<?php if(isset($product->product_type) && $product->product_type=="variable"): ?>
		<tr>
			<td colspan="4"> <?php echo woocommerce_variable_dropdown(); ?>  </td>
		</tr>
		<?php endif;?>
		<tr>
			<td> Our Price</td>
			<td> : </td>
			<td width="30%"> <?php //echo $k_add_to_cart_button; ?>  </td>
			<td> <?php  echo $k_add_to_cart_button; //woocommerce_simple_add_to_cart();?> </td>
		</tr>
		<tr>
			<td> Product Code </td>
			<td> : </td>
			<td> <span class="sku"> <?php echo $product->sku; ?> </span> </td>
			<td rowspan="2"> <?php k_add_to_wishlist_button();?>  </td>
		</tr>
		<tr>
			<td> Availability </td>
			<td> : </td>
			<td> <?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>  </td>
		</tr>
	</table>
</div>