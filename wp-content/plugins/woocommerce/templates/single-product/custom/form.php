<?php
/**
 * Simple product add to cart
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $woocommerce, $product, $post;
//var_dump($product->get_available_variations());
if ( ! $product->is_purchasable() ) return;
?>

<?php
	// Availability
	$availability = $product->get_availability();

	if ($availability['availability']) :
		echo apply_filters( 'woocommerce_stock_html', '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</p>', $availability['availability'] );
    endif;
?>

<?php if ( $product->is_in_stock() ) : ?>

	<?php do_action('woocommerce_before_add_to_cart_form'); ?>

				<?php if(isset($product->product_type) && $product->product_type=="variable"): ?>
				<tr>
					<td> Size</td>
					<td> : </td>
					<td width="30%"> &nbsp;   </td>
					<td> <?php echo woocommerce_variable_dropdown(); ?> </td>
				</tr>
				<?php endif;?>
				
	<form action="<?php echo esc_url( $product->add_to_cart_url() ); ?>" class="cart simple_cart_button_form" method="post" enctype='multipart/form-data'>

		<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="k_product_price_div" >
			<table>
				<tr>
					<td> Our Price</td>
					<td> : </td>
					<td width="30%"> <?php echo $product->get_price_html(); ?>  </td>
					<td> <?php woocommerce_simple_add_to_cart();?> </td>
				</tr>
				<tr>
					<td> Product Code </td>
					<td> : </td>
					<td> <span class="sku"> <?php echo $product->sku; ?> </span> </td>
					<td rowspan="2"> <?php k_add_to_wishlist_button();?>  </td>
				</tr>
				<tr>
					<td> Availability </td>
					<td> : </td>
					<td> <?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>  </td>
				</tr>
			</table>
		</div>
		

	</form>

	<?php do_action('woocommerce_after_add_to_cart_form'); ?>

<?php endif; ?>