<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $post, $product;
?>
<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="k_product_price_div" >
<?php  
$pagename = get_query_var('pagename');
if ( !$pagename && $id > 0 ) {
// If a static page is set as the front page, $pagename will not be set. Retrieve it from the queried object
$post = $wp_query->get_queried_object();
$pagename = $post->post_name;
}
  ?>

	<table class="html">
		<tr>
			<td width="100"><label> <?php echo __('[:en]Our Price[:fr]Notre Prix ','woocommerce')?> :</label></td>
			<td> <span class="price"> <?php echo $product->get_price_html(); ?> </span> </td>
		</tr>
		<tr>
			<td><label><?php echo __('[:en]Product Code[:fr]Code de Produit ','woocommerce')?> :</label> </td>
			<td> <span class="sku"> <?php echo $product->sku; ?> </span> </td>
		</tr>
		<tr>
			<td> <label><?php echo __('[:en]Availability[:fr]Disponibilité ','woocommerce')?> </label></td>
			<td colspan="2"> <span class="stock"><?php echo $product->is_in_stock() ? __('Stock','woocommerce') : __('Stock','woocommerce'); ?> </span>  </td>
		</tr>
		<tr>
			<td> <label> &nbsp;  </label></td>
			<td> <?php woocommerce_simple_add_to_cart();?> </td>
		</tr>
	</table>

</div>