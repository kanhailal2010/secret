<?php
/**
 * Related Products
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product, $woocommerce_loop;

$related = $product->get_related();

if ( sizeof( $related ) == 0 ) return;

//  for fetching related products (products with same category) ================= START
global $post;

$terms = get_the_terms( $post->ID, 'product_cat' );
$k_category = array();
foreach ($terms as $term) {
  $k_category[$term->term_id] = $term->term_id;
}

$ppp = 5;
if(array_key_exists(16,$k_category)) // 16 is category id of bossom buddies
{
	$ppp = 8; // for showing 8 related products for this category
}
$query_args = array( 'post__not_in' => array( $post->ID ), 'posts_per_page' => $ppp, 'no_found_rows' => 1, 'post_status' => 'publish', 'post_type' => 'product', 'tax_query' => array( 
    array(
      'taxonomy' => 'product_cat',
      'field' => 'id',
      'terms' => $k_category
    )));

//  for fetching related products (products with same category) ================= END

	/*
$args = apply_filters('woocommerce_related_products_args', array(
	'post_type'				=> 'product',
	'ignore_sticky_posts'	=> 1,
	'no_found_rows' 		=> 1,
	'posts_per_page' 		=> $ppp,
	'orderby' 			=> $orderby,
	'post__in' 				=> $related ,
	'post__not_in'			=> array($product->id) 
) );*/
$products = new WP_Query( $query_args );

$woocommerce_loop['columns'] 	= $columns;

echo "<style>";
//echo (count($products->posts) == 5 ) ? ".k_related_products .product { margin-left: 44px !important} " : ".k_related_products .product { margin-left: 80px !important} ";
echo (count($products->posts) == 5 ) ? ".k_related_products .product { width: 19% !important} " : ".k_related_products .product { width: 24% !important} ";
echo "</style>";
?>
<script type="text/javascript">
jQuery(function(){
	jQuery('.k_related_products li.product:last-child').css({borderRight:'0px solid',borderLeft:'0px solid #dfdbdf'});
	jQuery('.k_related_products li.product:last-child').prev().css({borderRight:'1px solid #dfdbdf'});
});
</script>
<?php
if ( $products->have_posts() ) : ?>
<br clear="all"/>
<div class="k_related_products_container">
	<h2 class="k_related_products_heading"><?php _e( 'Related Products', 'woocommerce' ); ?></h2>
	<div class="k_related_products">
		<?php woocommerce_product_loop_start(); ?>

			<?php while ( $products->have_posts() ) : $products->the_post(); ?>

				<?php 
					if(array_key_exists(16,$k_category)) // 16 is category id of bossom buddies
					woocommerce_get_template_part( 'content', 'b-related' );
					else
					woocommerce_get_template_part( 'content', 'k-related' );
				?>

			<?php endwhile; // end of the loop. ?>

		<?php woocommerce_product_loop_end(); ?>
	</div> <!-- .k_related_products -->
</div> <!-- .k_related_products_container -->
<br clear="all"/>

<?php endif;

wp_reset_postdata();