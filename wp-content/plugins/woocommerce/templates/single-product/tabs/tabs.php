<?php
/**
 * Single Product tabs
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Filter tabs and allow third parties to add their own
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );
?>
<div class="k_social_sharing">
<?php the_widget( 'MR_Social_Sharing_Toolkit_Widget'); ?> 
</div> <!-- .k_social_sharing -->
<?php
if ( ! empty( $tabs ) ) : ?>

	<div class="woocommerce-tabs">
		<ul class="tabs">
			<?php foreach ( $tabs as $key => $tab ) : ?>
				<?php if($key != "reviews"):?>
				<li class="<?php echo $key ?>_tab">
					<a href="#tab-<?php echo $key ?>"><?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', __($tab['title'],'woocommerce'), $key ) ?></a>
				</li>
				<?php endif;?>
			<?php endforeach; ?>
		</ul>
		<?php foreach ( $tabs as $key => $tab ) : ?>
			<?php if($key != "reviews"):?>
			<div class="panel entry-content" id="tab-<?php echo $key ?>">
				<?php  //var_dump($tab);   ?>
				<?php $tab['title'] = __($tab['title'],'woocommerce');   ?>
				<?php $tab['content'] = __($tab['content'],'woocommerce');   ?>
				<?php call_user_func( $tab['callback'], $key, $tab ); ?>
			</div>
			<?php endif;?>
		<?php endforeach; ?>
	</div>

<?php endif; ?>