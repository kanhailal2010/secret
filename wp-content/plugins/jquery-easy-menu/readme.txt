=== JQUERY DROPDOWN MENU ===
Contributors: extendyourweb.com
Donate link: http://www.extendyourweb.com/wordpress
Tags: sidebar, plugin, menu, horizontal, list, ul, li, CSS, custom-styles, dropdown, effects, jquery, menu, multi, navigation, options-page, pages, themes, widget
Requires at least: 2.8.0
Tested up to: 3.4
Stable tag: 1.2.1

Dropdown Menu Widget.

== Description ==

<a href="http://www.extendyourweb.com/wordpress/jquery-easy-menu/">Demo/Manual and plugin page</a>

With Dropdown Menu you can create and customize your vertical dropdown menu for your Wordpress site. 

This type of menu have a lot of advantatges: The most important probably is that you can go directly to the specific page without browsing all intermediate pages. A simply and fast way to surf the web.

Also, it have a lots of characteristics,so you can make various designs of your menus with a few clicks.

This are all of the features you can customize:

At the Main menu
<ul>
<li>Main menu Background colors</li>
<li>Buttons border </li>
<li>Buttons border color</li>
<li>Rounded border (or not)</li>
<li>Buttons separation</li>
<li>Mouse over background color</li>
<li>Font menu</li>
<li>Font menu size</li>
<li>Text menu color</li>
</ul>

At the submenus
<ul>
<li>Submenu width</li>
<li>Submenu Background colors</li>
<li>Submenu buttons border </li>
<li>Submenu buttons border color</li>
<li>Submenu bottons with founded border (or not)</li>
<li>Submenu buttons separation</li>
<li>Sumbenu mouse over background color</li>
<li>Font size submenu</li>
<li>Text submenu color</li>
</ul>

This widget works in all browsers.


If you want to add a menu without widget, first use the widget and edit the source code of the resulting page. Copy the source code from the comment: 
"EASY MENU JQUERY START" to the comment: "EASY MENU JQUERY END" and copy it as HTML where you want to have the menu.

== Installation ==

This section describes how to install the plugin and get it working.

1. Install the plugin via the plugins menu in your administrator.
2. Activate it and you'll see a new widget 'jquery easy menu'.
3. Place the widget on the position you want, select the menu you want to display and set the colors, sizes and other options that lets the widget.

== Frequently Asked Questions ==
= none =
none

== Screenshots ==


1. Menu sample.
2. Widget admin.



== Changelog ==

= 2.1 =
* Final version


= 1.1 =
* first release

== Upgrade Notice ==

= 2.1 = Final menu widget