
=========================================
=== WooCommerce Gift Certificates Pro ===
=========================================


Use this software at your own risk. In no event will the developers 
be liable for the result of using this software! No warranty is expressed 
nor implied.


See the changelog for all notes related to version updates.

READ ALL THE INFORMATION BELOW CAREFULLY and ensure you understand the
operation of this plugin before using it! 

=== LICENSE ===

This plugin comes with a single site license - unless you purchased
a multisite license. Please support us and help create more great plugins
by buying a license for each site that you use it on.

The software is distrbuted WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE. You use this software at your own risk.


INSTALLATION

	Upload the plugin to your plugins directory and activate it, like you
	would do for any other WordPress plugin.

	A notice will appear asking you to install the IgniteWoo Updater plugin ( if you
	have not already done so ). The plugin enables you to receive plugin updates and support.
	Click the link to install the Updater and activate the plugin. Then navigate to
	Dashboard -> IgniteWoo Licenses to add your license key, which you should have received
	via email. Your license keys are also listed on your My Account page at IgniteWoo.com

	http://ignitewoo.com/my-account


CUSTOMIZATION

	To customized the gift certificate email template, copy 'templates/voucher_email.php'
	to your theme directory and edit as you see fit. 
	
	For example, if you use TwentyTen theme, copy the file to
	'wp-content/themes/twentyten/templates/voucher_email.php'
    
USAGE

	This plugin adds a new meta box to all product editing pages.

	The meta box, labeled "Gift Certificate / Store Credit / Coupon Codes",
	allows you to optionally create gift certificates, store credits, or
	coupon codes when the product is purchased.

	Option fields allow you to control all necessary aspects of the offer.

	If you have enabled the product to provide a gift certificate, store credit,
	or coupon code, then when someone purchases the product and successfully pays
	for their purchase, the plugin will automatically generate the necessary
	codes and email them to the buyer. Optionally, during checkout, the buyer
	can define a recipient for the codes, in which case the codes would be emailed
	directly to recipient.

	The flexibility of the plugin allows you to:

	- Sell gift certificates / store credits as standalone product

	- Offer gift certificates / store credits / coupon codes with the purchase
	of a product.


	* To sell gift certificates as a standlone item, create a new product, enter
	a title, write a description, add images if you prefer, set the product type
	to SIMPLE, and check the box labeled "Enable gift certificates / coupons with
	this product", and configure the related options. Set the Discount Type to
	Gift Certificate / Store Credit and then set the Coupon Amount to be the amount
	the certificate should be worth. Set the other options as you see fit.

	Be careful if you choose to set an expiration date for gift certificates and
	store credits and check your local laws first! Some locales prohibit the
	expiration of "gift cards, gift certificates, store credits" etc. Overall, you
	should probably avoid setting an expiration date for gift certificates / store
	credits.

	* To provide gift certificates / store credits / coupon codes when a product is purchased,
	create a product as you normally would, check the box labeled "Enable gift
	certificates / coupons with this product", and configure the associated settings as you see fit.

	Note that if a product is enabled to provide gift certificates / store credits / coupon codes
	then ONE unique code will be generated for EACH quantity of that item in the order.
	So for example, if you have one product that is a gift certificate for $50 and
	someone buys 3 of them in their order, then 3 unique codes will be generated
	each worth $50. Likewise, if a product is enabled to provide a coupon code
	when the product is purchased, ONE unique code will be generated for EACH
	quantity of that item in the order.



*** IMPORTANT NOTES ***

	- WARNING: YOU CANNOT SELL STANDALONE GIFT CERTIFICATES / STORE CREDITS AS A VARIABLE
	PRODUCT TYPE! If you want to sell standalone gift certificates (e.g. gift certificates
	where the purpose of the product itself is to sell a gift certificate or store credit)
	then YOU MUST define those gift certificate product types as SIMPLE products. You can
	however add a gift certificate as an added value to a variable product type. Just keep
	in mind that you can only define one set of parameters for the gift certificate per
	product. This means that if you offer a gift certificate / store credit as added value
	to a variable product, then no matter which variation is purchased by a customer, the
	customer will receive a gift certificate / store credit in the amount defined by the
	Coupon Amount field in the product editor's related meta box.


	- Gift Certificates / Store Credit / Coupon Codes are generated immediately and sent
	to the recipient ( the buyer or the buyer's chosen recipient ) after the order status
	is set to Completed. Each code is sent in a seperate message, so the recipient will
	receive one email message per code that is generated. These messages are only sent
	to the recipient ONE TIME.


	- If you inadvertantly change an order status from Completed to Pending
	or any other default WooCommerce value other than Completed then the previously
	generated coupon codes will be unpublished and set to Draft status, thus effectively
	rendering them unusuable. When you change the order status back to Completed
	then those existing coupon codes will be re-published and immediately usable by
	whomever knows the codes. Note that email notice messages containing the codes are only
	sent ONE TIME, thus if you change an order from Complete to Pending, then back to
	Complete again, the notices are not resent again!

	- When you mark an order as Cancelled or Refunded, any gift certificate amount or store
	credit used to place the order is immediately refunded back to the corresponding
	voucher code, AND the voucher is re-marked as Published. This latter note means that if
	you set a gift certificate / store credit to be removed when the balance goes to zero,
	then that item will be put in the trash and not completed deleted, and when you cancel
	or refund an order then any related gift certificate / store credit codes will be
	taken out of the trash and republished. Thus you not delete coupons from the trash unless
	you're absolutely certain that they are not related to any order that might be refunded or
	cancelled! WARNING: This could possibly lead to loss of revenue IF you refund or cancel an
	order ( which results in a credit to the gift certificate / store credit ) and the customer
	then reuses the code to make another purchase ( whereupon the amount is deducted from the
	gift certificate / store credit balance ) and you later mark the previous order as Complete
	again - there may not be enough credit on the gift certificate / store credit code for you
	to recover your entire balance due! To safeguard against this condition, periodically review
	you coupon codes to determine if any have a negative balance. If any do have a negative
	balance then the customer owes you money.

	- WARNING: Note that coupon codes have the buyer or receipients email address attached
	to them. Therefore if you change the associated email address then whichever
	customer has the new email address will see the code and available credit
	on their My Account page! If you delete the associate email address entirely
	then no customer will see the code or available credit on their My Account page
	however this does not eliminate the ability for someone to use the code if they
	know what the code is and the code is in Published status.


	- NOTE: All gift certificates and coupon code are available for review and editing
	under the WooCommerce -> Coupon admin menu page. When viewing the list of coupons
	the Coupon Amount field will reflect the remaining balance available for Gift
	Certificate / Store Credit items.

TRANSLATORS

	Text domain is "ignitewoo_gift_certs"
	
	Place your language files in /wp-content/languages/woocommerce-gift-certs/ directory to avoid
	losing them during an upgrade.

	File name should be ignitewoo_gift_certs-en_US.mo, where en_US is your specific language.


=== END ===

