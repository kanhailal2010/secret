<?php if ( !defined('ABSPATH' ) ) die; ?>

<?php do_action('woocommerce_email_header', $heading ); ?>

<p><?php _e( 'This message is from', 'ignitewoo_gift_certs' )?><?php echo ' ' . $sitename; ?>!</p>

<p> <?php _e( 'Hi there', 'ignitewoo_gift_certs' ); ?> <?php echo $recipient_name ?>, </p>

<?php echo $message; ?>

<p><?php _e( 'To redeem your voucher use the following coupon code during checkout:', 'ignitewoo_gift_certs' ); ?></p>

<strong style="margin: 12px 0; font-size: 1.2em; font-weight: bold; display: block; text-align: center;">
	<?php echo $voucher_code; ?>
</strong>

<?php
	if ( '' != $voucher_expires ) {
		?>
		
		<p><em>
			
			<?php
				_e( 'Note that this voucher expires on ', 'ignitewoo_gift_certs' );
			
				echo $voucher_expires;
			?>

		</em></p>
		
		<?php 
	}
?>

<div style="clear:both;"></div>

<?php do_action( 'ignite_gc_qrcode', $order_id, $preview ) // this prints the QR code image when that option is enabled  ?>

<?php do_action('woocommerce_email_footer'); ?>