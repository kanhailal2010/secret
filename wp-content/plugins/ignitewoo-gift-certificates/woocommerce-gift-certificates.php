<?php
/*
Plugin Name: WooCommerce Gift Certificates Pro
Plugin URI: http://ignitewoo.com
Description: WooCommerce Gift Certificates Pro allows you to sell gift certificates / store credits / coupon codes as products in your store. You can also offer gift certificates / coupons as an additional item to an existing product.
Version: 2.4.3
Author: IgniteWoo.com
Author URI: http://ignitewoo.com

Copyright (c) 2012 IgniteWoo.com - All Rights Reserved

Single Site License - You may use this plugin on one site. Additional licenses must be purchased for use on additional sites.

*/
/**
* Required functions
*/
if ( ! function_exists( 'ignitewoo_queue_update' ) )
	require_once( dirname( __FILE__ ) . '/ignitewoo_updater/ignitewoo_update_api.php' );

$this_plugin_base = plugin_basename( __FILE__ );

add_action( "after_plugin_row_" . $this_plugin_base, 'ignite_plugin_update_row', 1, 2 );


/**
* Plugin updates
*/
ignitewoo_queue_update( plugin_basename( __FILE__ ), '000034507680e9da027946f01cc71467', '228' );

if ( !class_exists( 'Ignite_Gift_Certs' ) ) {
    
	class Ignite_Gift_Certs {

		var $admin = null;

		public function __construct() {

			add_action( 'init', array( &$this, 'load_plugin_textdomain' ) );

			// Check for mininum WooCommerce version
			add_action( 'init', array( &$this, 'software_tests' ), -1 );

			// Add coupon type
			add_filter( 'woocommerce_coupon_discount_types', array( &$this, 'add_coupon_code_type' ) );
			
			// Check code validity
			add_filter( 'woocommerce_coupon_is_valid', array( &$this, 'is_code_valid' ), 10, 2 );

			// Display meta box on product edit page and handle storing input data
			add_action( 'add_meta_boxes', array( &$this, 'add_meta_box' ), 12 );

			add_action( 'woocommerce_process_product_meta_simple', array( &$this, 'process_product_meta' ) );
			add_action( 'woocommerce_process_product_meta_variable', array( &$this, 'process_product_meta' ) );
			
			add_action( 'save_post', array( &$this, 'save_post_first' ), 1, 1 );
			add_action( 'save_post', array( &$this, 'save_original_post_title' ), 999, 1 );

			// Issue vouchers
			add_action( 'woocommerce_order_status_completed', array( &$this, 'add_vouchers' ), 20, 1 );

			// Credit a voucher if one is used and was previously refunded
			add_action( 'woocommerce_order_status_completed', array( &$this, 'debit_voucher' ), 20, 1 );
			add_action( 'woocommerce_order_status_pending', array( &$this, 'debit_voucher' ), 20, 1 );
			add_action( 'woocommerce_order_status_processing', array( &$this, 'debit_voucher' ), 20, 1 );
			add_action( 'woocommerce_order_status_on-hold', array( &$this, 'debit_voucher' ), 20, 1 );

			// Issue refunds when a voucher was used
			add_action( 'woocommerce_order_status_refunded', array( &$this, 'refund_vouchers' ), 20, 1 );
			add_action( 'woocommerce_order_status_cancelled', array( &$this, 'refund_vouchers' ), 20, 1 );

			// Deactivate vouchers
			add_action( 'woocommerce_order_status_pending', array( &$this, 'deactivate_vouchers' ), 20, 1 );
			add_action( 'woocommerce_order_status_processing', array( &$this, 'deactivate_vouchers' ), 20, 1 );
			add_action( 'woocommerce_order_status_on-hold', array( &$this, 'deactivate_vouchers' ), 20, 1 );
			add_action( 'woocommerce_order_status_failed', array( &$this, 'deactivate_vouchers' ), 20, 1 );
			
			// Add note to the public product page when the product comes with codes
			add_action( 'woocommerce_after_add_to_cart_button', array( &$this, 'show_product_vouchers' ), 9999999 );

			// Add forms to checkout page, verify form data entry
			add_action( 'woocommerce_checkout_shipping', array( &$this, 'recipient_detail_form' ), 999, 5 );
			add_action( 'woocommerce_before_checkout_process', array( &$this, 'verify_recipient_details' ), 11 );

			// Process new order data
			add_action( 'woocommerce_new_order', array( &$this, 'maybe_save_recipient_info' ), -1, 1 );

			// Remove gift certificates / store credits / coupon codes if config'd to do so
			add_action( 'woocommerce_new_order', array( &$this, 'maybe_update_or_remove' ), 10, 1 );

			// Update cart when a gift certificate or store credit is being used
			add_action( 'woocommerce_calculate_totals', array( &$this, 'adjust_cart_total' ) );

			// Add section to My Account page to show balance to customer when necessary
			add_action( 'woocommerce_before_my_account', array( &$this, 'show_customer_vouchers' ) );

			if ( defined( 'DOING_AJAX' ) )
				add_action( 'wp_ajax_ign_gc_preview', array( &$this, 'preview' ) );

			if ( is_admin() ) {

				add_action( 'woocommerce_shipping_init', array( &$this, 'init_gc_integration') );

			}

			
		}
		
		function load_plugin_textdomain() {

			$locale = apply_filters( 'plugin_locale', get_locale(), 'ignitewoo_gift_certs' );

			load_textdomain( 'ignitewoo_gift_certs', WP_LANG_DIR.'/woocommerce/ignitewoo_gift_certs-'.$locale.'.mo' );

			$plugin_rel_path = apply_filters( 'ignitewoo_translation_file_rel_path', dirname( plugin_basename( __FILE__ ) ) . '/languages' );

			load_plugin_textdomain( 'ignitewoo_gift_certs', false, $plugin_rel_path );

		}


		function preview() {
			global $ignite_gift_certs;

			if ( !wp_verify_nonce( $_POST['n'], 'ign_gc_preview' ) )
				die;

			$msg_details = array( 'voucher_to_email' => __( 'to@thisperson.com', 'ignitewoo_gift_certs' ),
						'voucher_from_email' => __( 'from@thisotherperson.com', 'ignitewoo_gift_certs' ),
						'voucher_from_name' => __( 'Mr. Sender', 'ignitewoo_gift_certs' ),
						'voucher_to_name' => __( 'Mr. Recipient', 'ignitewoo_gift_certs' ),
						'voucher_message' => __( "This would be the custom message text entered by the buyer and sent to the recipient as part of the overall email message from your store. ", 'ignitewoo_gift_certs' ),
					);
			echo $ignite_gift_certs->send_voucher( 'SAMPLECODE123', '100', 'ign_store_credit', '31 December 2020', $msg_details, 7777, true );

			die;
		}
		function init_gc_integration() {
		
			require_once( dirname( __FILE__ ) . '/class-admin.php' );
			
			$this->admin = new IgniteWoo_GC_Admin();
			
			require_once( dirname( __FILE__ ) . '/class-gc-reports.php' );
			
			$this->reports = new IgniteWoo_Event_Reports();
		}


		// Make sure the site is running WooCommerce 1.5 or later
		function software_tests() { 
			global $woocommerce;

			if ( !$woocommerce ) 
				return;

			if ( version_compare( $woocommerce->version, '1.5.0' ) <= 0 )
				add_action( 'admin_notices', array( &$this, 'version_nag' ) );

		}


		// Display notification when version requirement is not met
		function version_nag() { 
			global $woocommerce;

			echo '<div style="background-color:#cf0000;color:#fff;font-weight:bold;font-size:16px;margin: -1px 15px 0 5px;padding:5px 10px">';

			_e( 'The WooCommerce Gift Certificates Pro plugin requires WooCommerce 1.5.0 or newer to work correctly. You\'re using version', 'ignitewoo_gift_certs' );

			echo ' ' . $woocommerce->version; 

			echo '</div>';

		}

		
		// Add the new coupon type
		function add_coupon_code_type( $types ) {

			$types['ign_store_credit'] = __('Gift Certificate / Store Credit', 'ignitewoo_gift_certs' );

			return $types;
		}


		// Display a form to collect info about the voucher recipient
		function recipient_detail_form() {
			global $woocommerce;

			$maybe_show_form = false;
			
			foreach ( $woocommerce->cart->cart_contents as $product ) {

				$gc_enabled = get_post_meta( $product['product_id'], 'ignite_gift_enabled', true );

				if ( $gc_enabled )
					$maybe_show_form = true;

			}
			
			if ( $maybe_show_form == true ) {

				?>

				</div>
				</div>

				<div class="gift-certificate">
					<div class="receiver-form">

				<h3><?php _e( "Gift Certificate / Store Credit / Coupon receiver's details", 'ignitewoo_gift_certs' ) ?></h3>
				<p><?php _e( 'Leave the recipient name and email address blank to receive the voucher yourself. Or, enter details below to send the voucher(s) to someone else.', 'ignitewoo_gift_certs' ) ?></p>


				<p id="order_comments_field" class="form-row notes">
					<label class="" for="ign_receiver_name"><?php _e( 'Recipient Name', 'ignitewoo_gift_certs' ) ?></label>
					<input id="ign_receiver_name" type="text" name="ign_receiver_name" value="" />
				</p>

				<p id="order_comments_field" class="form-row notes">
					<label class="" for="ign_receiver_email"><?php _e( 'Recipient Email Address', 'ignitewoo_gift_certs' ) ?></label>
					<input id="ign_receiver_email" type="text" name="ign_receiver_email" value="" />
				</p>

				<p id="order_comments_field" class="form-row notes">
					<label class="" for="ign_receiver_message"><?php _e( 'Message to Recipient', 'ignitewoo_gift_certs' ) ?></label>
					<textarea rows="2" cols="5" id="ign_receiver_message" class="input-text" name="ign_receiver_message"></textarea>
				</p>

				<?php

			}
		}


		// Verify gift credit form details
		function verify_recipient_details() {
			global $woocommerce;

			$name_present = false;

			if ( isset( $_POST['ign_receiver_email'] ) && '' != $_POST['ign_receiver_email'] && !is_email( $_POST['ign_receiver_email'] ) ) {

				$woocommerce->add_error( __( "Error: The voucher recipient's email address is invalid.", 'ignitewoo_gift_certs' ) );

			}
				

			if ( isset( $_POST['ign_receiver_email'] ) && '' != trim( $_POST['ign_receiver_email'] ) ) { 

				if ( 	( isset( $_POST['ign_receiver_name'] ) && '' == trim( $_POST['ign_receiver_name'] ) ) || 
					( isset( $_POST['ign_receiver_name'] ) && '' != $_POST['ign_receiver_name'] && '' == trim( strip_tags( html_entity_decode( $_POST['ign_receiver_name'] ) ) ) )
				) {

					$woocommerce->add_error( __( "Error: You must enter the voucher recipient's name.", 'ignitewoo_gift_certs' ) );

				} else 
					$name_present = true;

			} 


			if ( $name_present && ( !isset( $_POST['ign_receiver_email'] ) || '' == trim( $_POST['ign_receiver_email'] ) )  ) {

				$woocommerce->add_error( __( "Error: You must enter the recipient's email address, or delete the recipient's name.", 'ignitewoo_gift_certs' ) );

			}


		}


		// Maybe add the voucher receiver's details to the order meta
		function maybe_save_recipient_info( $order_id ) {

			// Don't save anything unless the buyer specified a recipient
			if ( $_POST['billing_email'] == $_POST['ign_receiver_email'] )
				return;


			if ( isset( $_POST['ign_receiver_email'] ) && '' != trim( $_POST['ign_receiver_email'] ) ) {

				$email = trim( $_POST['ign_receiver_email'] );

				update_post_meta( $order_id, 'ign_receiver_email', $_POST['ign_receiver_email'] );

			}


			if ( isset( $_POST['ign_receiver_name'] ) && '' != trim( $_POST['ign_receiver_name'] ) ) {

				$name = trim( strip_tags( html_entity_decode( $_POST['ign_receiver_name'] ) ) );

				update_post_meta( $order_id, 'ign_receiver_name', $name );
			}


			if ( isset( $_POST['ign_receiver_message'] ) && '' != $_POST['ign_receiver_message'] ) {

				$msg = trim( strip_tags( html_entity_decode( $_POST['ign_receiver_message'] ) ) );

				update_post_meta( $order_id, 'ign_receiver_message', $msg );
			}

		}


		// Maybe show vouchers that are included with the product purchase
		function show_product_vouchers() {
			global $post, $woocommerce;

			$coupon = get_post_meta( $post->ID );

			if ( !$coupon || !is_array( $coupon ) || !$coupon['ignite_gift_enabled'][0] ) 
				return;

			echo '<div class="clear"></div>';

			echo '<div class="product_vouchers">';

			echo '<p>' . __( "This product comes with the following vouchers:", 'ignitewoo_gift_certs' );

			if ( 'ign_store_credit' == $coupon['discount_type'][0] )
				$value = __( 'Store credit of ', 'ignitewoo_gift_certs' ) . woocommerce_price( $coupon['coupon_amount'][0] );

			else if ( 'percent_product' == $coupon['discount_type'][0] )						
				$value = $coupon['coupon_amount'][0] . '%' . __( ' discount on a product.', 'ignitewoo_gift_certs' );
					
			else if ( 'percent' == $coupon['discount_type'][0] )
				$value = $coupon['coupon_amount'][0] . '%' . __( ' discount on your entire purchase.', 'ignitewoo_gift_certs' );

			else if ( 'fixed_cart' == $coupon['discount_type'][0] )
				$value = woocommerce_price( $coupon['coupon_amount'][0] ) . __( ' discount on your entire purchase.', 'ignitewoo_gift_certs' );

			else if ( 'fixed_product' == $coupon['discount_type'][0] )
				$value = woocommerce_price( $coupon['coupon_amount'][0] ) . __( ' discount on a product.', 'ignitewoo_gift_certs' );

			echo '<ul><li>' . $value ; 

			if ( '' != $coupon['expiry_date'][0] ) {

				echo ' &mdash; <span class="voucher_expires">';

				_e( 'Voucher expires:', 'ignitewoo_gift_certs' );

				echo ' ' . $coupon['expiry_date'][0];

				echo '<span>';
			}

			echo '</li></ul></div>';

		}


		// Check if a gift certificate / store credit is being used, and if so apply it to the cart totals
		function adjust_cart_total() {
			global $woocommerce;
		
			$woocommerce->cart->credit_used = array();
		
			if ( !$woocommerce->cart->applied_coupons )
				return;

			foreach ( $woocommerce->cart->applied_coupons as $cc ) {
				
				$coupon = new WC_Coupon( $cc );
				
				if ( !$coupon->is_valid() ) 
					continue;

				if (  'ign_store_credit' != $coupon->type )
					continue; 

				$total = $woocommerce->cart->cart_contents_total + $woocommerce->cart->tax_total + $woocommerce->cart->shipping_total + $woocommerce->cart->shipping_tax_total;
					
				if ( $coupon->amount > $total )
					$coupon->amount = $total;

				$woocommerce->cart->discount_total = $woocommerce->cart->discount_total + $coupon->amount;

				$woocommerce->cart->credit_used[ $cc ] = $coupon->amount;

			}

		}
		

		// Maybe update gift certificate / store credit balance and/or maybe delete coupon codes
		function maybe_update_or_remove( $order_id ) {
			global $woocommerce;

			if ( !$woocommerce->cart->applied_coupons )
				return;

			foreach( $woocommerce->cart->applied_coupons as $cc ) {

				$coupon = new WC_Coupon( $cc );

				if ( !$coupon )
					continue;

				$del_cc = get_post_meta( $coupon->id, 'ignite_delete_coupon', true );

				$del_gc = get_post_meta( $coupon->id, 'ignite_delete_gift_cert', true );

				if ( 'ign_store_credit' == $coupon->type ) {

					$credit_remaining = max( 0, ( $coupon->amount - $woocommerce->cart->credit_used[ $cc ] ) );
					
					if ( $credit_remaining <= 0 && 1 == $del_gc )
						wp_delete_post( $coupon->id );
					else
						update_post_meta( $coupon->id, 'coupon_amount', $credit_remaining );


					$codes_used = get_post_meta( $order_id, 'ign_store_credit_codes', true );

					if ( !is_array( $codes_used ) )
						$codes_used = array();

					$codes_used[] = array( 'code' => $cc, 'amount' => $woocommerce->cart->credit_used[ $cc ] );

					update_post_meta( $order_id, 'ign_store_credit_codes', $codes_used );

					
				} else if ( 1 == $del_cc ) { 

						wp_delete_post( $coupon->id );

				}

			}


		}
		

		// Return validity of the code used
		function is_code_valid( $valid, $coupon ) {
			global $woocommerce;

			if ( !$valid ) 
				return $valid;

			if ( 'ign_store_credit' != $coupon->type )
				return $valid; 

			if ( $coupon->amount <= 0 ) {

				$woocommerce->add_error( __('There is no balance remaining for the code you entered.', 'ignitewoo_gift_certs' ) );

				return false;
			}
				
			return $valid;
		}


		// Queue the addition of a metabox to the product editor
		function add_meta_box() { 

			add_meta_box( 'woocommerce-coupon-data', __('Gift Certificates / Store Credits / Coupon Codes', 'ignitewoo_gift_certs' ), array( &$this, 'product_options' ), 'product', 'normal', 'high' );

		}


		// Metabox display 
		function product_options() {
			global $post, $woocommerce;
			
			$saved_post = $post;

			if ( !file_exists( WP_PLUGIN_DIR . '/woocommerce/admin/post-types/writepanels/writepanel-coupon_data.php' ) ) {

				_e( '<strong><em>The required WooCommerce coupon data panel file is missing. This plugin needs an update!</em></strong>', 'ignitewoo_gift_certs' );

				return; 

			}

			if ( !function_exists( 'woocommerce_coupon_data_meta_box' ) )
				require_once( WP_PLUGIN_DIR . '/woocommerce/admin/post-types/writepanels/writepanel-coupon_data.php' );

			if ( !function_exists( 'woocommerce_coupon_data_meta_box' ) ) { 

				_e( '<strong><em>The required WooCommerce coupon data panel function is missing. This plugin needs an update!</em></strong>', 'ignitewoo_gift_certs' );

				return; 

			}

			$gift_cert_enabled = get_post_meta( $post->ID, 'ignite_gift_enabled', true );

			$delete_coupon_after_use = get_post_meta( $post->ID, 'ignite_delete_coupon', true );

			$delete_gift_cert_after_use = get_post_meta( $post->ID, 'ignite_delete_gift_cert', true );

			$restrict_to_buyer = get_post_meta( $post->ID, 'ignite_restrict_to_buyer', true );

			$prefix = get_post_meta( $post->ID, '_coupon_prefix', true );

			if ( '' != $prefix )
				$prefix = trim( $prefix );
			else
				$prefix = '';

			if ( $gift_cert_enabled ) 
				$checked = 'checked="checked"';
			else
				$checked = '';

			// WooCom global
			global $thepostid;

			$thepostid = $post->ID;

			echo '<p style="margin-left:10px;">';

			_e( 'You can add a gift certificate or coupon that is automatically generated and given the buyer when this product is purchased. To do that edit the fields below', 'ignitewoo_gift_certs' );

			echo '</p>';

			echo '
				<div id="coupon_options" class="panel woocommerce_options_panel">
				<div class="options_group">
					<p class="form-field">
						<label for="gc_enabled">Settings</label>
						<input type="checkbox" class="checkbox" name="ignite_enable_gift_cert" value="1" ' . $checked . '>  
						<span class="description">
			    ' . 			__( 'Enable gift certificates / store credits / coupon codes with this product.', 'ignitewoo_gift_certs' ) . '
							<img class="help_tip" data-tip="'. __('Enables gift certificates / coupon codes to be given to buyers of this product. NOTE: Disabling this does not delete or disable codes that have already been issued!', 'ignitewoo_gift_certs' ) . '" src="' . $woocommerce->plugin_url() . '/assets/images/tip.png" />

						</span>
					</p>
					<p class="form-field">
						<label for="gc_enabled" style="font-weight:bold">&nbsp;</label>
						<input type="text" name="_coupon_prefix" value="' . $prefix . '" style="width:45px">  
						<span class="description">
			    ' . 			__( 'Enter a gift certificate / coupon code prefix.', 'ignitewoo_gift_certs' ) . '
							<img class="help_tip" data-tip="'. __('Enter a brief prefix to use when generating new gift certificate / coupon codes to help you identify codes associated with this product when viewing the Coupons interface. Try to keep it short, 3 numbers and/or letters or less. Special characters are not allowed. The default is CC', 'ignitewoo_gift_certs' ) . '" src="' . $woocommerce->plugin_url() . '/assets/images/tip.png" />

						</span>
					</p>
				';

				if ( $delete_coupon_after_use ) 
					$checked = 'checked="checked"';
				else
					$checked = '';

				echo '
					<p class="form-field">
						<label for="gc_enabled" style="font-weight:bold">&nbsp;</label>
						<input type="checkbox" class="checkbox" name="woocommerce_delete_coupon_code_after_usage" value="1" ' . $checked . '>  
						<span class="description">
			    ' . 			__( 'Delete coupon code after one use. Otherwise codes can be resused until deleted manually or any defined expiration date is reached', 'ignitewoo_gift_certs' ) . '
						</span>
					</p>
				';

				if ( $delete_gift_cert_after_use ) 
					$checked = 'checked="checked"';
				else
					$checked = '';

				echo '
					<p class="form-field">
						<label for="gc_enabled" style="font-weight:bold">&nbsp;</label>
						<input type="checkbox" class="checkbox" name="woocommerce_delete_gift_credit_after_usage" value="1" ' . $checked . '>  
						<span class="description">
			    ' . 			__( 'Delete gift certificate / store credit code when the balance reaches zero', 'ignitewoo_gift_certs' ) . '
							<img class="help_tip" data-tip="'. __('Note that if an order was purchased using a gift certificate / stored credit, and that order is refunded or cancelled then any gift certificates / store credit amout used for the purchase will credited back to the related voucher code and those codes will be marked as published again and available for use by the customer even if those gift certificate / store credit codes were set to draft status or moved to the trash!', 'ignitewoo_gift_certs' ) . '" src="' . $woocommerce->plugin_url() . '/assets/images/tip.png" />
						</span>
					</p>
				';

				if ( $restrict_to_buyer ) 
					$checked = 'checked="checked"';
				else
					$checked = '';

				echo '
					<p class="form-field">
						<label for="gc_enabled" style="font-weight:bold">&nbsp;</label>
						<input type="checkbox" class="checkbox" name="woocommerce_restrict_to_buyer" value="1" ' . $checked . '>  
						<span class="description">
			    ' . 			__( 'Restrict use of this gift certificate to the user who purchases it', 'ignitewoo_gift_certs' ) . '
							<img class="help_tip" data-tip="'. __('Restrictions are based on the billing email address. When this feature is enabled, the user of the gift certificate must be logged in with the user account of the person who purchased it.', 'ignitewoo_gift_certs' ) . '" src="' . $woocommerce->plugin_url() . '/assets/images/tip.png" />
						</span>
					</p>
				';

			echo '</div></div>';

			echo '<p style="margin-left:10px">';

			_e( 'Set the default parameters for each gift certificate or coupon code that will be generated when this product is purchased', 'ignitewoo_gift_certs' );

			echo '</p>';

			echo '<p style="margin-left:10px; font-style:italic">';

			_e( 'NOTE: Changing the settings below will not change the settings of associated gift certificates / coupon codes that have already been issued!', 'ignitewoo_gift_certs' );

			echo '</p>';

			woocommerce_coupon_data_meta_box( $post );
			?>
			
			<style>
				#edit-slug-box { display:block; }
				#minor-publishing-actions { display: block; }
			</style>

			<?php 
			$post = $saved_post;

		}


		// Save metabox data
		function process_product_meta( $post_id ) {
			global $post; 

			if ( !function_exists( 'woocommerce_coupon_data_meta_box' ) )
				include_once( WP_PLUGIN_DIR . '/woocommerce/admin/post-types/writepanels/writepanel-coupon_data.php' );


			if ( isset( $_POST['ignite_enable_gift_cert'] ) && 1 == $_POST['ignite_enable_gift_cert'] ) 
				update_post_meta( $post_id, 'ignite_gift_enabled', 1 );
			else
				delete_post_meta( $post_id, 'ignite_gift_enabled' );


			if ( isset( $_POST['_coupon_prefix'] ) && '' != $_POST['_coupon_prefix'] )
				$prefix = ereg_replace("[^A-Za-z0-9]", "", $_POST['_coupon_prefix'] );

			if ( $prefix && '' != $prefix )
				update_post_meta( $post_id, '_coupon_prefix', $prefix );
			else
				update_post_meta( $post_id, '_coupon_prefix', 'CC' );


			if ( isset( $_POST['woocommerce_delete_coupon_code_after_usage'] ) && 1 == $_POST['woocommerce_delete_coupon_code_after_usage'] ) 
				update_post_meta( $post_id, 'ignite_delete_coupon', 1 );
			else
				delete_post_meta( $post_id, 'ignite_delete_coupon' );


			if ( isset( $_POST['woocommerce_delete_gift_credit_after_usage'] ) && 1 == $_POST['woocommerce_delete_gift_credit_after_usage'] ) 
				update_post_meta( $post_id, 'ignite_delete_gift_cert', 1 );
			else
				delete_post_meta( $post_id, 'ignite_delete_gift_cert' );

			if ( isset( $_POST['woocommerce_restrict_to_buyer'] ) && 1 == $_POST['woocommerce_restrict_to_buyer'] ) 
				update_post_meta( $post_id, 'ignite_restrict_to_buyer', 1 );
			else
				delete_post_meta( $post_id, 'ignite_restrict_to_buyer' );

			do_action( 'woocommerce_process_shop_coupon_meta', $post_id, $post );

		}

		
		function save_post_first( $post_id ) {
			global $post;
			
			if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
				return $post_id;

			if ( !isset( $post->post_type ) || 'product' != $post->post_type )
				return $post_id;
				
			remove_filter( 'woocommerce_coupon_code', 'strtolower' );
				
			$this->save_title = $_POST['post_title'];

			$this->edit_id = $post_id;

		}

		
		function save_original_post_title( $post_id ) {
			global $wpdb, $post;

			if ( !isset( $post->post_type ) || 'product' != $post->post_type )
				return;
				
			remove_filter( 'woocommerce_coupon_code', 'strtolower' );
			
			$sql = 'update ' . $wpdb->posts . ' set post_title="' . $this->save_title . '" where ID = ' . $this->edit_id;

			$wpdb->query( $sql );

		}
		
		// Generate a voucher code 
		function create_voucher( $order_id, $prefix, $meta, $email_addr, $restrict_to_buyer ) {

			// Create a semi-random unique ID code
			$voucher_code = uniqid( $prefix );

			$args = array(
					'post_title' 	=> $voucher_code,
					'post_content' 	=> '',
					'post_status' 	=> 'publish',
					'post_author' 	=> 1,
					'post_type'	=> 'shop_coupon'
			);
				
			$pid = wp_insert_post( $args );

			if ( !$pid || is_wp_error( $pid ) ) 
				return false;

			// Std WooCom meta
			update_post_meta( $pid, 'coupon_amount', $meta['coupon_amount'][0] );

			update_post_meta( $pid, 'discount_type', $meta['discount_type'][0] );

			update_post_meta( $pid, 'individual_use', $meta['individual_use'][0] );

			update_post_meta( $pid, 'apply_before_tax', $meta['apply_before_tax'][0] );

			update_post_meta( $pid, 'free_shipping', $meta['free_shipping'][0] );

			update_post_meta( $pid, 'product_ids', $meta['product_ids'][0] );

			update_post_meta( $pid, 'exclude_product_ids', $meta['exclude_product_ids'][0] );

			update_post_meta( $pid, 'usage_limit', $meta['usage_limit'][0] );

			update_post_meta( $pid, 'expiry_date', $meta['expiry_date'][0] );

			if ( $restrict_to_buyer )
				update_post_meta( $pid, 'customer_email', array( $email_addr ) );


			// Plugin's extra meta
			update_post_meta( $pid, 'associated_order_id', $order_id );

			update_post_meta( $pid, 'ignite_delete_coupon', $meta['ignite_delete_coupon'][0] );

			update_post_meta( $pid, 'ignite_delete_gift_cert', $meta['ignite_delete_gift_cert'][0] );
				
			return $voucher_code;
				
		}


		// Get existing "coupon" items based on type and order ID 
		function get_existing_vouchers( $type, $order_id ) { 

			$args = array(
					'post_type' => 'shop_coupon',
					'post_status' => $type,
					'posts_per_page' => -1,
					'meta_query' => array(
								array(
									'key' 	=> 'associated_order_id',
									'value' 	=> $order_id,
									'compare'	=> '='
								)
						)
				    );

			$existing_coupons = get_posts( $args );

			return $existing_coupons;

		}


		// Deduct amount from voucher when order status is changed, 
		function maybe_charge_voucher( $order_id ) { 
			global $wpdb;

			$refunded = get_post_meta( $order_id, 'coupon_refunded', true );

			// Not refunded, then don't re-debit the voucher
			if ( !$refunded ) 
				return;

			$codes_used = get_post_meta( $order_id, 'ign_store_credit_codes', true );

			if ( !$codes_used || !is_array( $codes_used ) )
				return;

			foreach( $codes_used as $key => $vals ) { 

				$sql = 'select ID from ' . $wpdb->posts . ' where post_type = "shop_coupon" and post_title = "' . $vals['code'] . '"';

				$coupon_id = $wpdb->get_var( $wpdb->prepare( $sql ) );

				if ( $coupon_id ) { 

					$balance = get_post_meta( $coupon_id, 'coupon_amount', true );

					if ( !$balance ) 
						$balance = 0;

					$balance = $balance - $vals['amount'];

					update_post_meta( $coupon_id, 'coupon_amount', $balance );

					delete_post_meta( $order_id, 'coupon_refunded' );

				}

			}

		}


		// Handle refund for the voucher
		function process_voucher_refund( $order_id ) {
			global $wpdb;

			// get amount used, add it back to the corresponding voucher post
			$codes_used = get_post_meta( $order_id, 'ign_store_credit_codes', true );

			if ( !$codes_used || !is_array( $codes_used ) || !count( $codes_used ) > 0 )
				return;

			foreach( $codes_used as $key => $vals ) { 

				$sql = 'select ID from ' . $wpdb->posts . ' where post_type = "shop_coupon" and post_title = "' . $vals['code'] . '"';

				$coupon_id = $wpdb->get_var( $wpdb->prepare( $sql ) );

				if ( $coupon_id ) { 

					$refunded = get_post_meta( $order_id, 'coupon_refunded', true );

					if ( !$refunded ) { 

						$coupon_balance = get_post_meta( $coupon_id, 'coupon_amount', true ); 

						if ( !$coupon_balance ) 
							$coupon_balance = 0;

						$coupon_balance = $coupon_balance + $vals['amount']; 

						update_post_meta( $order_id, 'coupon_refunded', 1 );

						update_post_meta( $coupon_id, 'coupon_amount', $coupon_balance );

						$wpdb->update( $wpdb->posts, array( 'post_status' => 'publish' ), array( 'ID' => $coupon_id ) );

					}

				}

			}

		}

		// Add or update a voucher
		function adjust_voucher( $coupon_prefix, $mode, $msg_details, $order_id, $product_id ) {
			global $wpdb;

			if ( !class_exists( 'WC_Coupon' ) )
				require_once( WP_PLUGIN_DIR . '/woocommerce/classes/class-wc-coupon.php' );

			$msg_details['voucher_to_email'] = trim( $msg_details['voucher_to_email'] );

			$restrict_to_buyer = get_post_meta( $product_id, 'ignite_restrict_to_buyer', true );

			$coupon = get_post_meta( $product_id );

			// gift cert and store credit codes
			if ( 'ign_store_credit' == $coupon['discount_type'][0] && 'add_voucher' == $mode ) {

				$existing_coupons = $this->get_existing_vouchers( 'draft', $order_id );

				if ( !is_wp_error( $existing_coupons) && count( $existing_coupons ) > 0 ) { 

					foreach( $existing_coupons as $key => $ec ) { 

						$wpdb->update( $wpdb->posts, array( 'post_status' => 'publish' ), array( 'ID' => $ec->ID ) );

						break;

					}

				} else { 

					$coupon_title = $this->create_voucher( $order_id, $coupon_prefix, $coupon, $msg_details['voucher_to_email'], $restrict_to_buyer );

					$this->send_voucher( $coupon_title, $coupon['coupon_amount'][0], $coupon['discount_type'][0], $coupon['expiry_date'][0], $msg_details, $order_id );

				}

			// regular coupon codes
			} else {

				if ( 'add_voucher' == $mode ) {

					$existing_coupons = $this->get_existing_vouchers( 'draft', $order_id );

					// Check for existing unpublished coupon codes and publish one if one exists
					// otherwise create a new one, publish it, and email it

					if ( !is_wp_error( $existing_coupons) && count( $existing_coupons ) > 0 ) { 

						foreach( $existing_coupons as $key => $ec ) { 

							$wpdb->update( $wpdb->posts, array( 'post_status' => 'publish' ), array( 'ID' => $ec->ID ) );

							break;

						}

					} else { 

						$amount = $coupon['coupon_amount'][0];

						$coupon_title = $this->create_voucher( $order_id, $coupon_prefix, $coupon, $msg_details['voucher_to_email'], $restrict_to_buyer );

						$this->send_voucher( $coupon_title, $coupon['coupon_amount'][0], $coupon['discount_type'][0], $coupon['expiry_date'][0], $msg_details, $order_id );

					}

				} elseif ( 'deactivate_voucher' == $mode ) {

					$existing_coupons = $this->get_existing_vouchers( 'publish', $order_id );

					if ( !is_wp_error( $existing_coupons) && count( $existing_coupons ) > 0 ) { 

						foreach( $existing_coupons as $key => $ec ) { 

							$ec->post_status = 'draft';

							$wpdb->update( $wpdb->posts, array( 'post_status' => 'draft' ), array( 'ID' => $ec->ID ) );

							break;

						}

					}

				} 
			
			}

			return $coupon_title;

		}
		

		// Process vouchers when order status changes
		function handle_vouchers( $order_id, $mode ) {

			$order = new WC_Order( $order_id );

			$meta = get_post_meta( $order_id );

			$order_items = ( array )$order->get_items(); 

			if ( !isset( $meta['ign_receiver_message'][0] ) )
				$msg = '';
			else
				$msg = trim( $meta['ign_receiver_message'][0] );

			$msg_details = array( 
					'voucher_from_name' => $order->billing_first_name . ' ' . $order->billing_last_name,
					'voucher_from_email' => $order->billing_email,
					'voucher_to_name' => $order->billing_first_name . ' ' . $order->billing_last_name,
					'voucher_to_email' => $order->billing_email,
					'voucher_message' => $msg
			);

			if ( isset( $meta['ign_receiver_email'][0] ) && is_email( $meta['ign_receiver_email'][0] ) ) {

				$msg_details['voucher_to_name'] = $meta['ign_receiver_name'][0];

				$msg_details['voucher_to_email'] = $meta['ign_receiver_email'][0];

			}

			foreach( $order_items as $item ) { 
				
				$product = $order->get_product_from_item( $item );

				$gc_enabled = get_post_meta( $product->id, 'ignite_gift_enabled', true );

				if ( !$gc_enabled )
					continue;
				
				$coupon_prefix = get_post_meta( $product->id, '_coupon_prefix', true );

				if ( !$coupon_prefix )
					$coupon_prefix = '';

				for( $x = 1; $x <= $item['qty']; $x++ ) { 

					$new_coupon = $this->adjust_voucher( $coupon_prefix, $mode, $msg_details, $order_id, $product->id );

				}

			}

		}


		// Add voucher codes when an order requires that due to purchase
		function add_vouchers( $order_id ) {

			$this->handle_vouchers( $order_id, 'add_voucher' );

		}


		// Remove voucher codes when if any were issued as a result of a purchase
		function deactivate_vouchers( $order_id ) {

			$this->handle_vouchers( $order_id, 'deactivate_voucher' );

		}

		// Refund voucher codes used during a purchase
		function refund_vouchers( $order_id ) {

			$this->process_voucher_refund( $order_id );

		}


		// Check to determine if a voucher needs to be debited, happens if an order was previously marked as refunded or cancelled
		function debit_voucher( $order_id ) { 

			$this->maybe_charge_voucher( $order_id ); 

		}


		function ignite_gc_qrcode( $order_number = '', $preview = false ) {

			if ( empty( $this->admin_settings ) )
				$this->admin_settings = get_option( 'woocommerce_woocommerce_gift_certificates_settings' );
		
			if ( 'yes' != $this->admin_settings['use_qr'] )
				return;
		
			require_once( dirname( __FILE__ ) . '/phpqrcode/phpqrcode.php' );

			if ( $preview )
				$url = admin_url( '/edit.php?post_type=shop_order' );
			else
				$url = admin_url( 'post.php?post=' . $order_id . '&action=edit' );
			
			ob_start();

			QRcode::png( $url );

			$out = ob_get_contents();

			ob_end_clean();

			?>

			<img class="ignitewoo_gc_qr_code" src="data:image/png;base64,<?php echo base64_encode( $out ); ?>">

			<?php 

		}

		
		// Send a msg to the recipient of the voucher code
		function send_voucher( $voucher_code, $amount, $type, $expires, $msg_details = array(), $order_id, $preview = false ) {
			global $woocommerce, $heading;

			if ( empty( $this->admin_settings ) )
				$this->admin_settings = get_option( 'woocommerce_woocommerce_gift_certificates_settings' );

			$sitename = get_option( 'blogname' );

			// handle special chars in the site name
			$sitename = wp_specialchars_decode( $sitename , ENT_QUOTES );

			$to = $msg_details['voucher_to_email'];

			$from = $msg_details['voucher_from_name'] . ' ( ' . $msg_details['voucher_from_email'] . ' )';
							
			$subject = $this->admin_settings['email_subject'];

			// maybe set intial option values
			if ( empty( $subject ) || !$subject ) {

				require_once( dirname( __FILE__ ) . '/class-admin.php' );
				
				$this->admin = new IgniteWoo_GC_Admin();
				
				$settings = $this->admin->init_form_fields();
				
				foreach( $this->admin->form_fields as $k => $v ) {
				
					if ( 'title' == $v['type'] )
						continue;
						
					if ( !get_option( 'woocommerce_' . $this->admin->id . $k ) )
						update_option( 'woocommerce_' . $this->admin->id . $k, $v['default'] );
						
				}
			}

			$subject = sprintf( $subject, $from );

			$message = '';

			if ( isset( $msg_details['voucher_to_name'] ) && '' != $msg_details['voucher_to_name'] ) 
				$recipient_name = $msg_details['voucher_to_name'];
			else 
				$recipient_name = '';

			// define WooCommmerce $heading var for use in the email template file
			if ( 'ign_store_credit' == $type )
				$heading =  sprintf( $this->admin_settings['heading_store_credit'], woocommerce_price( $amount ) );

			else if ( 'percent_product' == $type )
				$heading =  sprintf( $this->admin_settings['heading_percent_product'], $amount );

			else if ( 'percent' == $type )
				$heading =  sprintf( $this->admin_settings['heading_percent'], $amount );

			else if ( 'fixed_cart' == $type )
				$heading =  sprintf( $this->admin_settings['heading_fixed_cart'], woocommerce_price( $amount ) );

			else if ( 'fixed_product' == $type )
				$heading =  sprintf( $this->admin_settings['heading_fixed_product'], woocommerce_price( $amount ) );

			if ( isset( $msg_details['voucher_message'] ) && '' != trim( $msg_details['voucher_message'] ) ) {

				$message .= '<p>' . sprintf( $this->admin_settings['buyer_template_prefix'], $from ) . '</p>';

				$message .= wpautop( sprintf( $this->admin_settings['buyer_template'], $msg_details['voucher_message'] ) );


			}

			if ( '' != $expires )
				$voucher_expires = $expires;
			else
				$voucher_expires = '';

			if ( $preview ) {
				$mailer = $woocommerce->mailer(); // because we need hooks loaded
				$preview = true;
			}

			add_action( 'ignite_gc_qrcode', array( &$this, 'ignite_gc_qrcode' ) );
			
			$template = locate_template( array( 'templates/voucher_email.php' ), false, false );

			ob_start();

			if ( '' != $template ) 
				require ( $template );
			else 
				require( dirname( __FILE__ ) . '/templates/voucher_email.php' );

			$msg_body = ob_get_contents();

			ob_end_clean();

			if ( $preview ) {
				echo '<p style="margin-top: 10px"><span style="font-weight: bold">'; _e( 'Message Subject: ', 'ignitewoo_gift_certs' ); echo '</span>';
				echo $subject .'</p>';
				echo $msg_body;
				die;
			}

			woocommerce_mail( $to, $subject, $msg_body );

			if ( 'yes' ==  $this->admin_settings['email_admin'] ) {
			
				$store_admin = get_option( 'woocommerce_new_order_email_recipient', false );
				
				if ( !$store_admin )
					$store_admin = get_option( 'admin_email', false );
					
				if ( !empty( $store_admin ) ) {
					$subject = '[COPY] ' . $subject;
					woocommerce_mail( $store_admin, $subject, $msg_body );
				}
			}
		}


		// Maybe show user's gift certificates / store credit / coupon code and expiry date
		function show_customer_vouchers() {

			global $current_user;

			if ( !$current_user || !is_a( $current_user, 'WP_User' ) ) 
				$current_user = wp_get_current_user();

			$args = array(
					'post_type'		=> 'shop_coupon',
					'post_status'		=> 'publish',
					'posts_per_page' 	=> -1,
					'meta_query' 		=> array(
									array(
										'key'		=> 'customer_email',
										'value' 	=> $current_user->user_email,
										'compare'	=> 'LIKE'
									),
									array(
										'key'		=> 'coupon_amount',
										'value' 	=> '0',
										'compare'	=> '>=',
										'type'		=> 'NUMERIC'
									)
					)
				);
				
			$vouchers = get_posts( $args );

			if ( !$vouchers ) 
				return;

			?>

			<h2><?php _e('Store Vouchers', 'ignitewoo_gift_certs' ); ?></h2>

			<ul class="my_account_vouchers">

				<?php 

				foreach ( $vouchers as $code ) {

					$coupon = new WC_Coupon( $code->post_title );

					if ( 'ign_store_credit' == $coupon->discount_type )
						$value = __( 'Store credit of ', 'ignitewoo_gift_certs' ) . woocommerce_price( $coupon->amount );

					else if ( 'percent_product' == $coupon->discount_type )						
						$value = $coupon->amount . '%' . __( ' discount on a product.', 'ignitewoo_gift_certs' );
							
					else if ( 'percent' == $coupon->discount_type )
						$value = $coupon->amount . '%' . __( ' discount on your entire purchase.', 'ignitewoo_gift_certs' );

					else if ( 'fixed_cart' == $coupon->discount_type )
						$value = woocommerce_price( $coupon->amount ) . __( ' discount on your entire purchase.', 'ignitewoo_gift_certs' );

					else if ( 'fixed_product' == $coupon->discount_type )
						$value = woocommerce_price( $coupon->amount ) . __( ' discount on a product.', 'ignitewoo_gift_certs' );

					if ( '' != $coupon->expiry_date ) {

						$expires = ' &mdash; <span class="my_account_voucher_expires">' . 
							    __( 'Voucher expires:', 'ignitewoo_gift_certs' ) .
							    ' ' . $coupon->expiry_date . 
							    '<span>';
					}

					echo '<li><strong>Code: '. $coupon->code .'</strong> &mdash;'. $value  . $expires . '</li>';

				}

				?>

			</ul>

			<?php

		}

	}

}
global $ignite_gift_certs;
$ignite_gift_certs = new Ignite_Gift_Certs();

?>