<?php

/**

 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>

<!-- footer -->


<!--  Sales Banner Start  -->
<?php   $option = get_option('twentyeleven_theme_options'); ?>
<script>

function setCookie(cname,cvalue,ex_minutes)
{
var d = new Date();
d.setTime(d.getTime()+(ex_minutes*60*1000)); // in minutes
var expires = "expires="+d.toGMTString();
document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getdCookie(name) {
    var dc = document.cookie;
    var prefix = name + "=";
    var begin = dc.indexOf("; " + prefix);
    if (begin == -1) {
        begin = dc.indexOf(prefix);
        if (begin != 0) return null;
    }
    else
    {
        begin += 2;
        var end = document.cookie.indexOf(";", begin);
        if (end == -1) {
        end = dc.length;
        }
    }
    return unescape(dc.substring(begin + prefix.length, end));
} 

function getCookie(cname)
{
var name = cname + "=";
var ca = document.cookie.split(';');
for(var i=0; i<ca.length; i++) 
  {
  var c = ca[i].trim();
  if (c.indexOf(name)==0) return c.substring(name.length,c.length);
  }
return "";
}

function center_popup()
{
 var popup = jQuery('.popup');
 var popup_width = popup.width();
 var popup_height = popup.height();
 var sw = jQuery(window).width();
 var sh = jQuery(window).height();
 
 console.log(sw +" "+popup.width());
 var wspacing = ((sw-popup_width)/2);
 var hspacing = ((sh-popup_height)/2);
 popup.css({left: wspacing,top:hspacing});
}

jQuery(function(){
	center_popup();
	var salesCookie = getCookie("sales_banner");
	console.log("the cookie "+salesCookie);
	var active = "<?php echo (isset($option['sales_banner'])) ? $option['sales_banner'] : 0; ?>";	active = parseInt(active);	console.log("sales banner is  "+active);		if( active && salesCookie!=null && salesCookie!="none"){ jQuery('.popup').fadeIn(); }
	jQuery(window).resize(function(){ center_popup(); });
	// to close popup
	jQuery('.popup .close').click(function(){ jQuery('.popup').fadeOut(); setCookie('sales_banner','none',1);  });
});

function show_sales_banner() {
    var salesCookie = getCookie("sales_banner");
    if (salesCookie == null) {
        setCookie('sales_banner','block',5);
    }
}

show_sales_banner();
</script>
<?php 


function display_or_not()
{
	$display = 'none';	
	if(isset($option['sales_banner']) && $option['sales_banner'] ==0 )	
	{
		return $display;
	}	
	else if(isset($_COOKIE['sales_banner']))
	{
		$display = $_COOKIE['sales_banner'];
	}

	return $display;
/* if(is_page(978))
if(is_front_page() || is_page(978))
return 'block';
else */
	// to display on front page only	if(is_front_page())	return $display;	else 	return 'none';
}
/*echo " the don (". display_or_not().") <br/>";
echo " the coookie  (". $_COOKIE['sales_banner'].")";
echo "the cookie ".var_dump($_COOKIE);*/
?>
<style>
.popup { position: fixed; background: #fff; top: 0; left: 0; padding: 12px;
box-shadow : 0 0 9px 6px #000;
-moz-box-shadow : 0 0 9px 6px #000;
-webkit-box-shadow : 0 0 9px 6px #000;
z-index: 999;
display: <?php echo display_or_not(); ?>;
}
.relative { position: relative; }
.close { position: absolute; top: -32px; right: -32px; }
</style><?php if(is_front_page()): ?>
<div class="popup" >
	<div class="relative">
		<img class="close" src="<?php echo bloginfo('template_url')?>/images/wrong.png">
	</div> <!-- .relative	-->
		<?php 
		if(isset($_REQUEST['lang']) && $_REQUEST['lang']=='fr')
		echo do_shortcode('[rev_slider sales_banner_french]');
		else 
		echo do_shortcode('[rev_slider sales_banner_english]');
	?>
</div> <!-- .popup --><?php endif;?>
<!--  Sales Banner End  -->


<div class="footer_outer">

<div class="footer">

<div class="clear"></div>

<div class="footer_left_box">
	<div class="first_box">
		<div class="email_box_div">
		<?php dynamic_sidebar('email'); ?>
		<br clear="all"/>
		</div>
		<div class="copy_right_text"><a href="#">Copyright  &copy;  2013 ALL RIGHTS RESERVED Itsasecret.co</a> </div>
		<div align="center" class="pay_pal"><img src="<?php bloginfo('template_url'); ?>/images/paypal.png"  alt=""/></div>
	</div>
</div>

<div class="footer_text_box">

<div class="footer_heading"><?php echo __('[:en]COMPANY INFO[:fr]NOTRE ENTREPRISE','woocommerce'); ?> </div>

<div class="footer_text">

<!--



-->

<?php wp_nav_menu( array('menu' => 'footer-menu' )); ?>

<?php // wp_nav_menu('footer-menu'); ?>

</div>

</div>

<div class="footer_text_box">

<div class="footer_heading"><?php echo __('[:en]MY ACCOUNT[:fr] MON COMPTE','woocommerce'); ?></div>

<div class="footer_text">

<?php wp_nav_menu( array('menu' => 'footer-one' )); ?>

</div>

</div>

<div class="footer_text_box">

<div class="footer_heading"><?php echo __('[:en]SOCIAL[:fr]RÉSEAUX SOCIAUX','woocommerce'); ?></div>

<div class="footer_text">

<!--<ul>

<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/fb.png" alt="" />Facebook</a></li>

<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/twitter.png" alt="" />Twitter</a></li>

<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/gplus.png" alt="" />Google +</a></li>

<li><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/rss.png" alt="" />Rss</a></li>





</ul> -->

<?php dynamic_sidebar('socials');?>

</div>

</div>

<div class="footer_text_box">

<div class="footer_heading"><?php echo __('[:en]CUSTOMER SERVICE[:fr] SERVICE À LA CLIENTÈLE');?></div>

<div class="footer_text">

<?php wp_nav_menu( array('menu' => 'footer-three' )); ?>

</div>

</div>

<div class="clear"></div>

</div>

</div>

<!-- footer_close -->

</div>

<?php wp_footer(); ?>



</body>

</html>