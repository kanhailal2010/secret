<?php

// Default options values
$banner_options = array(
	'banner_code' 				=> 'Enter Banner shortcode',
	'banner_active' 			=> 0,
	'banner_code_vertical'		=> '',
	'banner_vertical_active'	=> 0,
	'banner_code_fullwidth'		=> '',
	'banner_fullwidth_active'	=> 0,
);

if ( is_admin() ) : // Load only if we are viewing an admin page

function banner_register_settings() {
	// Register settings and call sanitation functions
	// register_setting( $option_group, $option_name, $sanitize_callback );
	register_setting( 'theme_banner_options', 'banner_options', 'banner_validate_options' );
}

add_action( 'admin_init', 'banner_register_settings' );

function theme_banner_options() {
	// Add theme options page to the addmin menu
	//add_theme_page( $page_title, $menu_title, $capability, $menu_slug, $function );
	add_theme_page( 'Banner Options', 'Banner Options', 'edit_theme_options', 'banner_options', 'banner_theme_options_page' );
}

add_action( 'admin_menu', 'theme_banner_options' );

// Function to generate options page
function banner_theme_options_page() {
	global $banner_options;

	if ( ! isset( $_REQUEST['settings-updated'] ) )
		$_REQUEST['settings-updated'] = false; // This checks whether the form has just been submitted. ?>

	<div class="wrap">

	<?php screen_icon(); echo "<h2>" . get_current_theme() . __( ' Theme Options' ) . "</h2>";
	// This shows the page's name and an icon if one has been provided ?>

	<?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
	<div class="updated fade"><p><strong><?php _e( 'Options saved' ); ?></strong></p></div>
	<?php endif; // If the form has just been submitted, this shows the notification ?>

	<form method="post" action="options.php">

	<?php $settings = get_option( 'banner_options', $banner_options ); ?>
	
	<?php settings_fields( 'theme_banner_options' );
	/* This function outputs some hidden fields required by the form,
	including a nonce, a unique number used to ensure the form has been submitted from the admin page
	and not somewhere else, very important for security */ ?>

	<table class="form-table"><!-- Grab a hot cup of coffee, yes we're using tables! -->

		<tr valign="top"><th scope="row"><label for="banner_code">Banner Shortcode</label></th>
			<td>
				<input id="banner_code" name="banner_options[banner_code]" type="text" value="[:en][rev_slider sales_banner_english][:fr][rev_slider sales_banner_french]<?php // esc_attr_e($settings['banner_code']); ?>" style="width: 400px;padding: 3px;" />
			</td>
			<th scope="row"> <label for="banner_active">Activate </label> 	</th>
			<td>
				<input type="radio" name="banner_options[banner_active]" value="1" <?php echo ($settings['banner_active']=='1') ? 'checked="checked"' : ''; ?> > Yes 
				<input type="radio" name="banner_options[banner_active]" value="0" <?php echo ($settings['banner_active']=='0') ? 'checked="checked"' : ''; ?> > No 
			</td>
		</tr>
		<!-- For Vertical banner -->
		<tr valign="top"><th scope="row"><label for="banner_code_vertical">Vertical Banner Shortcode</label></th>
			<td>
				<input id="banner_code_vertical" name="banner_options[banner_code_vertical]" type="text" value="[:en][rev_slider vertical_english][:fr][rev_slider vertical_french] <?php // esc_attr_e($settings['banner_code_vertical']); ?>" style="width: 400px;padding: 3px;" />
			</td>
			<th scope="row"> <label for="banner_vertical_active">Activate </label> 	</th>
			<td>
				<input type="radio" name="banner_options[banner_vertical_active]" value="1" <?php echo ($settings['banner_vertical_active']=='1') ? 'checked="checked"' : ''; ?> > Yes 
				<input type="radio" name="banner_options[banner_vertical_active]" value="0" <?php echo ($settings['banner_vertical_active']=='0') ? 'checked="checked"' : ''; ?> > No 
			</td>
		</tr>

		<tr valign="top"><th scope="row"><label for="banner_code_fullwidth">Full width Banner Shortcode</label></th>
			<td>
				<input id="banner_code_fullwidth" name="banner_options[banner_code_fullwidth]" type="text" value="[:en][rev_slider fullpage_english][:fr][rev_slider fullpage_french]<?php // esc_attr_e($settings['banner_code_fullwidth']); ?>" style="width: 400px;padding: 3px;" />
			</td>
			<th scope="row"> <label for="banner_fullwidth_active">Activate </label> 	</th>
			<td>
				<input type="radio" name="banner_options[banner_fullwidth_active]" value="1" <?php echo ($settings['banner_fullwidth_active']=='1') ? 'checked="checked"' : ''; ?> > Yes 
				<input type="radio" name="banner_options[banner_fullwidth_active]" value="0" <?php echo ($settings['banner_fullwidth_active']=='0') ? 'checked="checked"' : ''; ?> > No 
			</td>
		</tr>
	</table>

	<p class="submit"><input type="submit" class="button-primary" value="Save Options" /></p>

	</form>

	</div>

	<?php
}

function banner_validate_options( $input ) {
	global $banner_options;

	$settings = get_option( 'banner_options', $banner_options );
	/*
	// We strip all tags from the text field, to avoid vulnerablilties like XSS
	$input['banner_code'] = wp_filter_nohtml_kses( $input['footer_copyright'] );
	
	// We strip all tags from the text field, to avoid vulnerablilties like XSS
	$input['active'] = wp_filter_post_kses( $input['intro_text'] );*/

	return $input;
}

endif;  // EndIf is_admin()